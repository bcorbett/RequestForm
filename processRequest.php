<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Software Order</title>
   <style type="text/css">
    label {
      font-weight: bold;
    }
  </style>
</head>

<body>
  <fieldset style="width: 50em; margin-left: auto; margin-right: auto;">
    <legend>
      <h1>Order Confirmation</h1>
    </legend>
    <?php
      echo "<strong>First Name:</strong>";
      echo trim($_POST['firstName']);
      echo "<br/><br/>";
      echo "<strong>Last Name:</strong>";
      echo trim($_POST['lastName']);
      echo "<br/><br/>";
      echo "<strong>Email Address:</strong>";
      echo trim($_POST['email']);
      echo "<br/><br/>";
      echo "<strong>Method of Shipping:</strong>";
      echo $_POST['ship'];
      echo "<br/><br/>";
      echo "<strong>Software Order:</strong>";
      echo "<br/><table border=\"1\" cellpadding=\"1\"><tr><th>Software</th><th>Cost</th></tr>";
      include 'softwares.php';
      foreach ($_POST['choice'] as $sel) {
        echo "<tr><td>";
        echo $sel;
        echo "</td><td>$";
        echo $softwares[$sel];
        echo "</td></tr>";
      }
      echo "<tr><td>Total</td><td>$";
      echo sum($softwares);
      echo "</td></tr></table><br/><strong>Order Specifications:</strong>";
      echo "<pre style=\"margin: 0px\"><em>";
      echo $_POST['spec'];
      echo "</em></pre>";
     ?>
     <?php
      function sum($arr) {
	$total = 0;
        foreach ($_POST['choice'] as $sel)
          $total = $total + $arr[$sel];
        return $total;
      }
     ?>
  </fieldset>
</body>
</html>
