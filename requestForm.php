<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Software Order</title>
  <style type="text/css">
    label {
      font-weight: bold;
    }
  </style>
</head>

<body>
  <fieldset style="width: 50em; margin-left: auto; margin-right: auto;">
    <legend>
      <h1>Order Request Form</h1>
    </legend>
    <form action="processRequest.php" method="post">
      <p>
        <label for="firstName">First Name:</label> <input type="text" name="firstName"/>
        <br/><br/>
        <label for="lastName">Last Name:</label> <input type="text" name="lastName"/>
        <br/><br/>
        <label for="email">Email Address:</label> <input type="email" name="email" placeholder="domanin@website.com" required/>
        <br/><br/>
        <label for="ship">Method of Shipping:</label>
        <input type="radio" name="ship" value="UPS"/> UPS&nbsp;
        <input type="radio" name="ship" value="FedEX"/>FedEX&nbsp;
        <input type="radio" name="ship" value="US Mail" checked="checked"/>US Mail&nbsp;
        <input type="radio" name="ship" value="Other"/>Other
        <br/><br/>
        <label for="softwares">Softwares:</label>
        <br/>
        <?php
          require 'softwares.php';
        ?>
        <select name="choice[]" multiple="multiple">
          <?php
            foreach ($softwares as $key => $value)
            echo "<option value=\"$key\">$key (\$$value)</option>";
          ?>
        </select>
        <br/>
        <br/>
        <label for="spec">Order Specifications:</label>
        <br/>
        <textarea rows="10" cols="100" name="spec"></textarea>
      </p>

      <p>
        <input type="reset" value="Reset" />
        <input type="submit" value="Submit" />
      </p>
    </form>
  </fieldset>
</body>
</html>
